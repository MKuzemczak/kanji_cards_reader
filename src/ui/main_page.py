from tkinter import Frame, Menu
from tkinter.filedialog import askopenfilename

from ..services.kanji_data_manager import KanjiDataManager
from .button_area import ButtonArea
from .colors import *
from .kanji_card_display_area import KanjiCardDisplayArea
from .labeled_entry import LabeledEntry


class MainPage(Frame):
    def __init__(self, master, **options):
        super().__init__(master, **options)

        self.menubar = Menu(master)
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Import kanji data", command=self.import_kanji_data_callback)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=self.master.quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.master.config(menu=self.menubar)

        self.card_display = KanjiCardDisplayArea(self, bg=WINDOW_BG)
        self.card_display.grid(row=0, column=0, columnspan=2, sticky="nsew", padx=40)

        self.entry = LabeledEntry(
            master=self, label_text="Card range", allowed_chars="1234567890-", width=50, bg=WINDOW_BG
        )
        self.entry.grid(sticky="ns", row=1, column=0)

        self.first_stage_display_entry = LabeledEntry(
            master=self, label_text="Display at first stage", text="all", width=50, bg=WINDOW_BG
        )
        self.first_stage_display_entry.grid(sticky="ns", row=1, column=1)

        self.buttons = ButtonArea(
            self,
            prev_command=self.prev_button_click_callback,
            next_command=self.next_button_click_callback,
            random_command=self.random_button_click_callback,
            show_command=self.show_button_click_callback,
            bg=WINDOW_BG
        )
        self.buttons.grid(sticky="ns", row=2, column=0, columnspan=2, pady=60)
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(0, weight=1)
        self.data_manager = KanjiDataManager()

        self.current_card = None

    def import_kanji_data_callback(self):
        filename = askopenfilename()
        if filename == '':
            return
        self.data_manager.load_data_form_json(filename, show_progress=True)

    def prev_button_click_callback(self):
        range_start, range_end = self.get_range()
        self.current_card = self.data_manager.get_previous_kanji(self.current_card, range_start, range_end)
        self.display_current_card_first_stage()

    def next_button_click_callback(self):
        range_start, range_end = self.get_range()
        self.current_card = self.data_manager.get_next_kanji(self.current_card, range_start, range_end)
        self.display_current_card_first_stage()

    def random_button_click_callback(self):
        range_start, range_end = self.get_range()
        self.current_card = self.data_manager.get_random_kanji(range_start, range_end)
        self.display_current_card_first_stage()

    def show_button_click_callback(self):
        self.card_display.display(self.current_card)
        self.buttons.random_button["state"] = "normal"

    def display_current_card_first_stage(self):
        included = self.get_included_list()
        if "all" not in included:
            self.buttons.random_button["state"] = "disabled"
        self.card_display.display(self.current_card, included=self.get_included_list())

    def get_included_list(self):
        return [s.strip() for s in self.first_stage_display_entry.get_text().split(",")]

    def get_range(self):
        range_start = 0
        range_end = -1
        text = self.entry.get_text()
        rang = text.split("-")

        if len(rang) == 2:
            range_start = 0 if not rang[0] else int(rang[0])
            range_end = -1 if not rang[1] else int(rang[1])

        return range_start, range_end
