from tkinter import Frame, LabelFrame

from .colors import *
from .fonts import *
from .table.table import Table


class TablePanel(Frame):
    def __init__(
        self, master=None, header_text="", header_font=(BASIC_UI_FONT_NAME, 15), table_columns=[],
        content_font=(BASIC_UI_FONT_NAME, 15), content_width=-1, content_height=-1, content_expand=True,
        content_fill="both", **options
    ):
        super().__init__(master, **options)
        self.config(bg=FRAME_BG)

        self.labelframe = LabelFrame(
            self, text=header_text, bg=FRAME_BG, bd=0, fg=WINDOW_FG, font=header_font, height=content_height
        )
        self.labelframe.pack(side="right", expand=True,
                             fill="both", padx=20, pady=5)

        self.table = Table(
            self.labelframe, columns=table_columns, column_minwidths=[200, 200, 1000], header_font=content_font,
            cell_font=content_font, height=content_height, frame_background=FRAME_BG
        )
        self.table.pack(padx=10, pady=10, fill="both", expand=True)

    def insert_row(self, data):
        self.table.insert_row(data)

    def clear(self):
        self.table.clear()
