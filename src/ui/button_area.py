from tkinter import Button, Frame

from .colors import *


class ButtonArea(Frame):
    def __init__(
        self, master=None, prev_command=None, next_command=None, random_command=None, show_command=None, **options
    ):
        super().__init__(master, **options)

        self.prev_button = Button(
            self, text="Previous", bd=0, width=30, height=3, bg=BUTTON_BG, fg=WINDOW_FG, command=prev_command
        )
        self.prev_button.config(font=("Segoe UI", 15))
        self.prev_button.grid(padx=20, sticky="nsew", row=0, column=0)
        self.next_button = Button(
            self, text="Next", bd=0, width=30, height=3, bg=BUTTON_BG, fg=WINDOW_FG, command=next_command
        )
        self.next_button.config(font=("Segoe UI", 15))
        self.next_button.grid(padx=20, sticky="nsew", row=0, column=1)
        self.random_button = Button(
            self, text="Random", bd=0, width=30, height=3, bg=BUTTON_BG, fg=WINDOW_FG, command=random_command
        )
        self.random_button.config(font=("Segoe UI", 15))
        self.random_button.grid(padx=20, sticky="nsew", row=0, column=2)

        self.show_button = Button(
            self, text="Show", bd=0, width=30, height=3, bg=BUTTON_BG, fg=WINDOW_FG, command=show_command
        )
        self.show_button.config(font=("Segoe UI", 15))
        self.show_button.grid(padx=20, sticky="nsew", row=0, column=3)

        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_columnconfigure(3, weight=1)
