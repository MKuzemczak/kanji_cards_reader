from tkinter import Frame

from .colors import *
from .fonts import *
from .display_panel import DisplayPanel
from .table_panel import TablePanel


class KanjiCardDisplayArea(Frame):
    def __init__(self, master=None, **options):
        super().__init__(master, **options)

        self.kanji_panel = DisplayPanel(
            master=self, header_text="Kanji #0", content_text="あ", content_font=(BASIC_UI_FONT_NAME, 100)
        )
        self.kanji_panel.grid(row=0, column=0, rowspan=2, sticky="nsew", padx=10, pady=10)

        self.kun_panel = DisplayPanel(master=self, header_text="Kun reading", content_width=10)
        self.kun_panel.grid(row=0, column=1, sticky="nsew", padx=10, pady=10)

        self.on_panel = DisplayPanel(master=self, header_text="On reading", content_width=10)
        self.on_panel.grid(row=0, column=2, sticky="nsew", padx=10, pady=10)

        self.meaning_panel = DisplayPanel(master=self, header_text="Meaning", content_font=("Segoe UI", 13))
        self.meaning_panel.grid(row=1, column=1, columnspan=2, sticky="nsew", padx=10, pady=10)

        self.usages_panel = TablePanel(
            master=self, header_text="Usage examples", content_height=200, table_columns=["Word", "Reading", "Meaning"])
        self.usages_panel.grid(row=2, column=0, columnspan=3, sticky="nsew", padx=10, pady=10)

        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)

    def display(self, card, included=["all"]):
        self.clear_all()
        if "all" in included or "kanji" in included:
            self.kanji_panel.set_content_text(card.kanji)

        if "all" in included or "on" in included:
            self.on_panel.set_content_text(card.on)

        if "all" in included or "kun" in included:
            self.kun_panel.set_content_text(card.kun)

        if "all" in included or "meaning" in included:
            self.meaning_panel.set_content_text(card.meaning)

        if "all" in included or "id" in included:
            self.set_kanji_label_id(card.id)

        if "all" in included or "examples" in included:
            for e in card.examples:
                self.usages_panel.insert_row([e.word, e.reading, e.meaning])

    def set_kanji_label_id(self, number):
        self.kanji_panel.set_header_text("Kanji #" + str(number))

    def clear_all(self):
        self.kanji_panel.set_content_text("")
        self.on_panel.set_content_text("")
        self.kun_panel.set_content_text("")
        self.meaning_panel.set_content_text("")
        self.set_kanji_label_id(0)

        rows = self.usages_panel.table.number_of_rows
        for i in reversed(range(rows)):
            self.usages_panel.table.delete_row(i)
