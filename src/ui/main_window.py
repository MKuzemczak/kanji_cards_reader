from tkinter import Tk

from .colors import *
from .main_page import MainPage


class MainWindow:
    def __init__(self, master=Tk()):
        self.master = master
        master.state("zoomed")
        master.title("Kanji cards reader")
        master.configure(bg=WINDOW_BG)

        self.main_page = MainPage(self.master, bg=WINDOW_BG)
        self.main_page.pack(fill="x", expand=True)

    def open(self):
        self.master.mainloop()
