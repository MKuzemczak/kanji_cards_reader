from tkinter import Frame, LabelFrame

from .colors import *
from .fonts import *
from .wrapping_label import WrappingLabel


class DisplayPanel(Frame):
    def __init__(
        self, master: Frame = None, header_text="", header_font=(BASIC_UI_FONT_NAME, 15), content_text="",
        content_font=(BASIC_UI_FONT_NAME, 20), content_width=-1, content_height=-1, content_expand=True,
        content_fill="both", master_relative_width=-1, master_relative_height=-1, **options
    ):
        super().__init__(master, **options)
        self._master = master
        self._master_relative_width = master_relative_width
        self._master_relative_height = master_relative_height
        self.bind('<Configure>', self.update_relative_size)

        self.config(bg=FRAME_BG)
        self.labelframe = LabelFrame(self, text=header_text, bg=FRAME_BG, bd=0, fg=WINDOW_FG, font=header_font)
        self.labelframe.pack(side="right", expand=True, fill="both", padx=20, pady=5)

        label_args = {}
        if content_height > 0:
            label_args["height"] = content_height
        if content_width > 0:
            label_args["width"] = content_width

        self.content_label = WrappingLabel(
            self.labelframe, text=content_text, bg=FRAME_BG, fg=WINDOW_FG, font=content_font,
            **label_args
        )
        self.content_label.pack(expand=content_expand, fill=content_fill)

    def set_header_text(self, text):
        self.labelframe.config(text=text)

    def set_content_text(self, text):
        self.content_label.config(text=text)

    def update_relative_size(self, e):
        if self._master_relative_width > 0:
            self.config(width=self._master.winfo_width() * self._master_relative_width)
        if self._master_relative_height > 0:
            self.config(width=self._master.winfo_height() * self._master_relative_height)
