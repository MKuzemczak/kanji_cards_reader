from tkinter import Entry, Frame, Label

from .colors import *
from.fonts import *


class LabeledEntry(Frame):
    def __init__(self, master=None, label_text="", text="", allowed_chars="", **options):
        super().__init__(master=master, **options)

        self._allowed_chars = allowed_chars
        self.label = Label(self, text=label_text, bg=WINDOW_BG, fg=WINDOW_FG, font=(BASIC_UI_FONT_NAME, 10))
        self.label.pack(side="left")

        vcmd = (self.register(self.on_validate),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

        self.entry = Entry(self, validate="key", validatecommand=vcmd)
        self.entry.pack(side="left")
        self.entry.insert(0, text)

    def on_validate(self, d, i, P, s, S, v, V, W):
        if self._allowed_chars:
            for c in S:
                if c not in self._allowed_chars:
                    self.bell()
                    return False
        return True

    def get_text(self):
        return self.entry.get()
