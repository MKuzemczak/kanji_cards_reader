from tkinter import Frame, Label
from tkinter.constants import *

from .cell import Cell


class HeaderCell(Cell):
    def __init__(
        self, master, text, bordercolor=None, borderwidth=1, padx=0, pady=0, background=None, foreground=None,
        font=None, anchor=CENTER, separator=True
    ):
        Cell.__init__(
            self, master, background=background, highlightbackground=bordercolor, highlightcolor=bordercolor,
            highlightthickness=borderwidth, bd=0
        )
        self.pack_propagate(False)

        self._header_label = Label(
            self, text=text, background=background, foreground=foreground, font=font)
        self._header_label.pack(padx=padx, pady=pady, expand=True)

        if separator and bordercolor is not None:
            separator = Frame(self, height=2, background=bordercolor,
                              bd=0, highlightthickness=0, class_="Separator")
            separator.pack(fill=X, anchor=anchor)

        self.update()
        height = self._header_label.winfo_reqheight() + 2 * padx
        width = self._header_label.winfo_reqwidth() + 2 * pady

        self.configure(height=height, width=width)
