from tkinter import Label
from tkinter.constants import *

from .cell import Cell


class DataCell(Cell):
    def __init__(
        self, master, variable, anchor=W, bordercolor=None, borderwidth=1, padx=0, pady=0, background=None,
        foreground=None, font=None
    ):
        Cell.__init__(
            self, master, background=background, highlightbackground=bordercolor, highlightcolor=bordercolor,
            highlightthickness=borderwidth, bd=0
        )

        self._message_widget = Label(
            self, textvariable=variable, font=font, background=background, foreground=foreground
        )
        self._message_widget.pack(
            expand=True, padx=padx, pady=pady, anchor=anchor)
