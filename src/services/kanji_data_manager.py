from tkinter import *
from tkinter.ttk import *
import json
import random
import threading

from ..database_utils.database_service import DatabaseService
from ..excpetions import exceptions
from ..models.kanji_usage_example import KanjiUsageExample
from ..models.kanji_card import KanjiCard


class KanjiDataManager:
    def __init__(self):
        self.path = ''
        self.data_not_learned = []
        self.data_learned = []
        self.database = DatabaseService("dbfile.db")
        self.database.create()
        self.data = self.database.get_all_kanji_cards()
        for c in self.data:
            if c.learned > 0:
                self.data_learned.append(c)
                continue
            self.data_not_learned.append(c)

    def load_data_from_dicts(
        self, dicts, progress_bar_window=None, progress_bar=None, label=None
    ):
        cntr = 0
        for d in dicts:
            examples = []
            for e in d.get("examples", []):
                examples.append(KanjiUsageExample(
                    e.get("word", ""), e.get("reading", ""), e.get("meaning", "")
                ))
            card = KanjiCard(
                d.get("number"), d.get("kanji", ""), d.get("kun", ""),
                d.get("on", ""), d.get("meaning", ""), examples, 0
            )
            self.data.append(card)
            self.data_not_learned.append(card)
            self.database.insert_kanji_card(card)
            if progress_bar is not None and progress_bar_window is not None:
                progress_bar["value"] = cntr * 100 / len(dicts)
                cntr += 1
                progress_bar_window.update_idletasks()
        if (label is not None and progress_bar is not None and progress_bar_window is not None):
            label.config(text="Complete")
            progress_bar["value"] = 100
            progress_bar_window.update_idletasks()

    def load_data_form_json(self, path, show_progress=False):
        self.path = path
        file_handle = open(self.path, "r", encoding="utf8")
        file_data = file_handle.read()
        dicts = json.loads(file_data)
        self.database.clean_all_tables()
        self.data = []
        load_window = None
        label = None
        pbar = None

        if show_progress:
            load_window = Tk()
            load_window.title("Import data")
            label = Label(load_window, text='Importing data...', anchor='w')
            label.pack(fill='both', padx=10, pady=10)
            pbar = Progressbar(load_window, orient="horizontal", length=500, mode="determinate")
            pbar.pack(padx=10, pady=10)

        y = threading.Thread(target=self.load_data_from_dicts, args=(dicts, load_window, pbar, label))
        y.start()

        if show_progress:
            load_window.mainloop()

    def get_random_kanji(self, range_start=0, range_end=-1):
        self.check_data_empty(self.get_random_kanji.__name__)
        start, end = self.validate_range(range_start, range_end)
        if end < 0:
            end = len(self.data)

        return random.choice(self.data[start:end])

    def get_random_not_learned_kanji(self):
        self.check_data_empty(self.get_random_not_learned_kanji.__name__)
        return random.choice(self.data_not_learned)

    def get_next_kanji(self, card, range_start=0, range_end=-1):
        self.check_data_empty(self.get_next_kanji.__name__)
        start, end = self.validate_range(range_start, range_end)
        if end == -1:
            end = len(self.data) - 1
        if card is None:
            return self.data[start]
        if card.id < self.data[start].id:
            return self.data[start]
        if card.id >= self.data[end].id:
            return self.data[end]
        for i in range(start, end + 1):
            if self.data[i].id == card.id:
                return self.data[i + 1]

        return None

    def get_previous_kanji(self, card, range_start=0, range_end=-1):
        self.check_data_empty(self.get_previous_kanji.__name__)
        start, end = self.validate_range(range_start, range_end)
        if end == -1:
            end = len(self.data) - 1
        if card is None:
            return self.data[end]
        if card.id <= self.data[start].id:
            return self.data[start]
        if card.id > self.data[end].id:
            return self.data[end]
        for i in range(start, end + 1):
            if self.data[i].id == card.id:
                return self.data[i - 1]

        return None

    def mark_kanji_learned(self, kanji_id):
        self.check_data_empty(self.mark_kanji_learned.__name__)
        for i in range(len(self.data_not_learned)):
            c = self.data_not_learned[i]
            if c.id == kanji_id:
                c.learned = 1
                self.data_not_learned.pop(i)
                self.data_learned.append(c)
                self.database.update_kanji_card_learned(c.id, c.learned)
                break

    def check_data_empty(self, calling_method_name=""):
        if len(self.data) == 0:
            raise exceptions.KanjiDataEmpty(
                "Attempted to use empty KanjiDataManager, in method: " + calling_method_name
            )

    def validate_range(self, start, end):
        ret_start = start if start >= 0 and start < len(self.data) else 0
        ret_end = end if end < len(self.data) and end > 0 and end >= start else -1

        return ret_start, ret_end
