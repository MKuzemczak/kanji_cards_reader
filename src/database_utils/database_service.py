import os
import sqlite3
from contextlib import contextmanager

from ..excpetions import exceptions
from ..models.kanji_card import KanjiCard
from ..models.kanji_usage_example import KanjiUsageExample


class DatabaseService:
    def __init__(self, path=''):
        self.path = path
        self.is_created = False

    @contextmanager
    def connection(self):
        f = open(self.path, 'a+')
        f.close()
        conn = sqlite3.connect(self.path)
        try:
            with conn:
                yield conn
        finally:
            conn.close()

    def create(self):
        with self.connection() as conn:
            cursor = conn.cursor()

            cursor.execute('''CREATE TABLE IF NOT EXISTS kanji_cards
                                (id INTEGER PRIMARY KEY NOT NULL,
                                kanji TEXT NOT NULL,
                                kun_reading TEXT NOT NULL,
                                on_reading TEXT NOT NULL,
                                meaning TEXT NOT NULL,
                                learned INTEGER NOT NULL)''')

            cursor.execute('''CREATE TABLE IF NOT EXISTS kanji_usage_examples
                                (id INTEGER PRIMARY KEY NOT NULL,
                                word TEXT NOT NULL,
                                reading TEXT NOT NULL,
                                meaning TEXT NOT NULL,
                                kanji_card_id REFERENCES kanji_cards(id) NOT NULL)''')

            cursor.execute('''CREATE TRIGGER IF NOT EXISTS after_kanji_card_delete
                                AFTER DELETE ON kanji_cards
                                BEGIN
                                    DELETE FROM kanji_usage_examples WHERE kanji_card_id = OLD.id;
                                END;''')

        self.is_created = True

    def assert_database_created(self):
        if not self.is_created:
            raise exceptions.DBNotCreatedException("Database is used but has not been created")

    def get_table_names(self):
        self.assert_database_created()

        with self.connection() as conn:
            cursor = conn.cursor()

            cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
            return [t[0] for t in cursor.fetchall()]

    def insert_kanji_card(self, card):
        self.assert_database_created()

        with self.connection() as conn:
            cursor = conn.cursor()

            cursor.execute(
                "INSERT INTO kanji_cards VALUES (?,?,?,?,?,?)",
                [card.id, card.kanji, card.kun, card.on, card.meaning, card.learned]
            )
            for e in card.examples:
                cursor.execute('''INSERT INTO kanji_usage_examples(word,reading,meaning,kanji_card_id)
                            VALUES (?,?,?,?)''', [e.word, e.reading, e.meaning, card.id])

    def get_all_kanji_cards(self):
        self.assert_database_created()

        result = []
        with self.connection() as conn:
            cursor = conn.cursor()

            prev_kanji_card_id = -1
            query = """SELECT c.id, c.kanji, c.kun_reading, c.on_reading,
                c.meaning, c.learned, e.word, e.reading, e.meaning
                FROM
                (kanji_cards c INNER JOIN kanji_usage_examples e ON c.id = e.kanji_card_id)"""
            for r in cursor.execute(query):
                if not (r[0] == prev_kanji_card_id):
                    result.append(KanjiCard(r[0], r[1], r[2], r[3], r[4], [], r[5]))
                    prev_kanji_card_id = r[0]
                if len(result) > 0:
                    result[-1].examples.append(KanjiUsageExample(r[6], r[7], r[8]))
        return result

    def get_not_learned_kanji_cards(self):
        self.assert_database_created()

        result = []
        with self.connection() as conn:
            cursor = conn.cursor()

            prev_kanji_card_id = -1
            query = """SELECT c.id, c.kanji, c.kun_reading, c.on_reading,
                c.meaning, c.learned, e.word, e.reading, e.meaning
                FROM
                (kanji_cards c INNER JOIN kanji_usage_examples e ON c.id = e.kanji_card_id)
                WHERE c.learned = 0"""
            for r in cursor.execute(query):
                if not (r[0] == prev_kanji_card_id):
                    result.append(KanjiCard(r[0], r[1], r[2], r[3], r[4], [], r[5]))
                if len(result) > 0:
                    result[-1].examples.append(KanjiUsageExample(r[6], r[7], r[8]))
        return result

    def update_kanji_card_learned(self, kanji_card_id, learned=1):
        self.assert_database_created()

        with self.connection() as conn:
            cursor = conn.cursor()

            cursor.execute("UPDATE kanji_cards SET learned = ? WHERE id = ?", [learned, kanji_card_id])

    def remove_database_file(self):
        os.remove(self.path)

    def clean_all_tables(self):
        self.assert_database_created()

        with self.connection() as conn:
            cursor = conn.cursor()

            cursor.execute("DELETE FROM kanji_cards")
            cursor.execute("DELETE FROM kanji_usage_examples")
