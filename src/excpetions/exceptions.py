class DBNotCreatedException(Exception):
    """Exception raised, when database is used but has not been created"""
    pass


class KanjiDataEmpty(Exception):
    """Exception raised, when trying tu use KanjiDataManager data extraction methods while it's empty"""
    pass
