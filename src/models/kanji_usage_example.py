from dataclasses import dataclass


@dataclass
class KanjiUsageExample:
    word: str
    reading: str
    meaning: str
