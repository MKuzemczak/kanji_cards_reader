from dataclasses import dataclass
from typing import List

from .kanji_usage_example import KanjiUsageExample


@dataclass
class KanjiCard:
    id: int
    kanji: str
    kun: str
    on: str
    meaning: str
    examples: List[KanjiUsageExample]
    learned: int = 0
