import json
import pytest

from src.database_utils.database_service import DatabaseService
from src.services.kanji_data_manager import KanjiDataManager


@pytest.fixture(scope='module')
def mocked_database(module_mocker):
    def mock_init(*args, **kwargs):
        pass

    module_mocker.patch.object(DatabaseService, "__init__", mock_init)
    module_mocker.patch.object(DatabaseService, "create")
    module_mocker.patch.object(DatabaseService, "insert_kanji_card")
    module_mocker.patch.object(DatabaseService, "update_kanji_card_learned")
    module_mocker.patch.object(DatabaseService, "clean_all_tables")
    module_mocker.patch.object(DatabaseService, "get_all_kanji_cards")
    DatabaseService.get_all_kanji_cards.return_value = []


@pytest.fixture
def empty_manager(mocked_database):
    mgr = KanjiDataManager()
    return mgr


def test_load_data_form_json(empty_manager, testing_json, kanji_card_in_test_json, dict_in_test_json, mocker):
    mocker.patch.object(json, "loads")
    json.loads.return_value = [dict_in_test_json]
    empty_manager.load_data_form_json(testing_json)
    assert len(empty_manager.data) == 1
    empty_manager.database.insert_kanji_card.assert_called_with(empty_manager.data[0])
    assert kanji_card_in_test_json == empty_manager.data[0]


@pytest.fixture(scope='module')
def manager(testing_json, mocked_database):
    mgr = KanjiDataManager()
    mgr.load_data_form_json(testing_json)
    return mgr


def test_get_random_kanji(manager, kanji_card_in_test_json):
    card = manager.get_random_kanji()
    assert card == kanji_card_in_test_json


def test_random_not_learned_kanji(manager, kanji_card_in_test_json):
    card = manager.get_random_not_learned_kanji()
    assert card.learned == 0


def test_mark_kanji_learned(manager, kanji_card_in_test_json):
    manager.mark_kanji_learned(kanji_card_in_test_json.id)
    marked_kanji = next(card for card in manager.data if card.id == kanji_card_in_test_json.id)
    manager.database.update_kanji_card_learned.assert_called_with(marked_kanji.id, marked_kanji.learned)
    assert marked_kanji.learned == 1
