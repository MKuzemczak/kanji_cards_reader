import pytest

from src.database_utils.database_service import DatabaseService
from src.excpetions.exceptions import DBNotCreatedException


@pytest.fixture
def database(tmp_path):
    filename = tmp_path / 'test_db.db'
    f = open(filename, "a+")
    f.close()
    db = DatabaseService(str(filename))
    db.create()
    return db


def test_create(database):
    with database.connection() as conn:
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM sqlite_master WHERE type = 'table'")
        result = cursor.fetchall()
        assert len(result) == 2


def test_insert_kanji_card_db_not_created(kanji_card_ones_not_learned):
    database = DatabaseService()
    with pytest.raises(DBNotCreatedException):
        database.insert_kanji_card(kanji_card_ones_not_learned)


def query_all_kanji_cards(database):
    with database.connection() as conn:
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM kanji_cards")
        return cursor.fetchall()


def query_all_kanji_usage_examples(database):
    with database.connection() as conn:
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM kanji_usage_examples")
        return cursor.fetchall()


def test_insert_kanji_card(database, kanji_card_ones_not_learned):
    database.insert_kanji_card(kanji_card_ones_not_learned)
    card_expect = [1, "1", "1", "1", "1", 0]
    example_expect = [1, "1", "1", "1", 1]

    result = query_all_kanji_cards(database)
    assert len(result) == 1
    for i in range(len(card_expect)):
        assert card_expect[i] == result[0][i]

    result = query_all_kanji_usage_examples(database)
    assert len(result) == 1
    for i in range(len(example_expect)):
        assert example_expect[i] == result[0][i]


def test_get_all_kanji_cards_db_not_created():
    database = DatabaseService()
    with pytest.raises(DBNotCreatedException):
        database.get_all_kanji_cards()


def test_get_all_kanji_cards(database, kanji_card_ones_not_learned):
    database.insert_kanji_card(kanji_card_ones_not_learned)
    result = database.get_all_kanji_cards()
    assert len(result) == 1
    assert result[0] == kanji_card_ones_not_learned


def test_get_not_learned_kanji_cards_db_not_created():
    database = DatabaseService()
    with pytest.raises(DBNotCreatedException):
        database.get_not_learned_kanji_cards()


def test_get_not_learned_kanji_cards(database, kanji_card_twos_learned, kanji_card_ones_not_learned):
    database.insert_kanji_card(kanji_card_twos_learned)
    database.insert_kanji_card(kanji_card_ones_not_learned)
    result = database.get_not_learned_kanji_cards()
    assert len(result) == 1
    assert result[0] == kanji_card_ones_not_learned


def test_update_kanji_card_learned_db_not_created():
    database = DatabaseService()
    with pytest.raises(DBNotCreatedException):
        database.update_kanji_card_learned(0)


def test_update_kanji_card_learned(database, kanji_card_ones_not_learned, kanji_card_ones_learned):
    expected = kanji_card_ones_learned
    database.insert_kanji_card(kanji_card_ones_not_learned)
    database.update_kanji_card_learned(kanji_card_ones_not_learned.id, 1)
    result = database.get_all_kanji_cards()
    assert len(result) == 1
    assert result[0] == expected
