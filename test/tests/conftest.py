import pytest
import sys


dir_ = '../../'
if dir_ not in sys.path:
    sys.path.append(dir_)


from src.models.kanji_card import KanjiCard  # noqa: E302
from src.models.kanji_usage_example import KanjiUsageExample  # noqa: E302


# --- pytest hooks ---

def pytest_assertrepr_compare(op, left, right):
    if isinstance(left, KanjiCard) and isinstance(right, KanjiCard):
        return ["they're different, my boii", str(left), "is different than", str(right)]

    return None

# --- end of pytest hooks ---


@pytest.fixture(scope="session")
def testing_json():
    return "../test_data/test_kanji_entry.json"


@pytest.fixture(scope="session")
def kanji_card_in_test_json():
    return KanjiCard(1, "1", "1", "1", "1", [KanjiUsageExample("1", "1", "1")], 0)


@pytest.fixture(scope="session")
def dict_in_test_json():
    return {
        'number': 1, 'kanji': '1', 'kun': '1', 'on': '1', 'meaning': '1',
        'examples': [{'word': '1', 'reading': '1', 'meaning': '1'}]
    }


@pytest.fixture(scope="session")
def kanji_card_ones_not_learned():
    return KanjiCard(1, "1", "1", "1", "1", [KanjiUsageExample("1", "1", "1")], 0)


@pytest.fixture(scope="session")
def kanji_card_ones_learned():
    return KanjiCard(1, "1", "1", "1", "1", [KanjiUsageExample("1", "1", "1")], 1)


@pytest.fixture(scope="session")
def kanji_card_twos_learned():
    return KanjiCard(2, "2", "2", "2", "2", [KanjiUsageExample("2", "2", "2")], 1)
